const express = require('express');
const router = express.Router();
const { getFile, getFiles, createFile, deleteFile, editFile } = require('./../controllers/fileController');
const { validateFile } = require('./../middlewares/validateFile');
const { checkContent } = require('./../middlewares/checkContent');
const { setPassword, checkPassword } = require('./../middlewares/protectWithPassword');

router.post('/files', validateFile, checkContent, setPassword, createFile);

router.get('/files', getFiles);

router.get('/files/:filename', checkPassword, getFile);

router.delete('/files/:filename', deleteFile);

router.put('/files/:filename', checkContent, editFile);

module.exports = router;
