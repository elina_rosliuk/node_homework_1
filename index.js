const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const fileRouter = require('./routers/fileRouter');

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api', fileRouter);

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});
