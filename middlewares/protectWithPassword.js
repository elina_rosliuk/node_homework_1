const fs = require('fs').promises;
const path = require('path');

const passwordPath = path.join('protectedFiles.json');

module.exports.setPassword = async (req, res, next) => {
    const { filename, password } = req.body;

    if (req.body.hasOwnProperty('password') && !password && password.length < 6) {
        return res
            .status(400)
            .json({ message: 'Please, enter a correct password that has more than 6 characters' });
    }

    const passwordData = JSON.parse(await fs.readFile(passwordPath, 'utf8'));

    passwordData[filename] = password;

    const updatedPasswordData = JSON.stringify(passwordData);

    if (password) {
        await fs.writeFile(passwordPath, updatedPasswordData, 'utf8');
    }

    next();
};

module.exports.checkPassword = async (req, res, next) => {
    const { filename } = req.params;
    const { password } = req.query;

    const passwordData = JSON.parse(await fs.readFile(passwordPath, 'utf8'));
    const currentFilePassword = passwordData[filename];

    if (currentFilePassword && !password) {
        return res.status(400).json({ message: `Please, enter a password to unlock the file ${filename}` });
    }

    if (currentFilePassword && currentFilePassword !== password) {
        return res.status(400).json({ message: 'Incorrect password' });
    }

    next();
};
