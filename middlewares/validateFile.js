module.exports.validateFile = async (req, res, next) => {
    const { filename } = req.body;

    const isValide = filename.match(/log|txt|json|yaml|xml|js$/);

    if (!isValide) {
        return res.status(400).json({ message: 'File extension is not supported' });
    }

    next();
}