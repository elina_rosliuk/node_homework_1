const fs = require('fs').promises;
const path = require('path');

const dirname = 'uploadedFiles';
const filesPath = (filename) => path.join(dirname, filename);

const handleDirectory = async () => {
    await fs
        .access(path.join('./', dirname))
        .then(() => true)
        .catch(() => fs.mkdir(path.join('./', dirname)));
};

module.exports.createFile = async (req, res) => {
    const { filename, content } = req.body;

    if (!filename) {
        return res.status(400).json({ message: `Please specify 'filename' parameter` });
    }

    await handleDirectory();

    try {
        await fs.writeFile(filesPath(filename), content, 'utf8');
    } catch (err) {
        return res.status(500).json({ message: 'Server error' });
    }

    res.status(200).json({ message: 'File created successfully' });
};

module.exports.getFiles = async (_, res) => {
    let files;

    try {
        files = await fs.readdir(dirname, 'utf8');
    } catch (err) {
        return res.status(400).json({ message: 'Client error' });
    }

    res.status(200).json({ message: 'Success', files });
};

module.exports.getFile = async (req, res) => {
    const { filename } = req.params;
    let content;

    try {
        content = await fs.readFile(filesPath(filename), 'utf8');
    } catch (err) {
        return res.status(400).json({ message: `No file with '${filename}' filename found` });
    }

    const { birthtime } = await fs.stat(filesPath(filename));

    const extension = path.extname(filename).slice(1);

    res.status(200).json({
        message: 'Success',
        filename,
        content,
        extension,
        uploadedData: birthtime,
    });
};

module.exports.deleteFile = async (req, res) => {
    const { filename } = req.params;

    try {
        await fs.unlink(filesPath(filename));
    } catch (err) {
        return res.status(400).json({ message: `No file with '${filename}' filename found` });
    }

    res.status(200).json({ message: 'File was successfully deleted' });
};

module.exports.editFile = async (req, res) => {
    const { filename } = req.params;
    const { content } = req.body;

    try {
        await fs.writeFile(filesPath(filename), content, 'utf8');
    } catch (err) {
        return res.status(500).json({ message: 'Server error' });
    }

    res.status(200).json({ message: 'File was successfully updated' });
};
